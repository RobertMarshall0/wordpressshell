#!/usr/bin/env bash
# Fox WordPress Base - v1.0.0
# -----------------------------------------------------------------------------

# Run checker to make sure the user can actually do this
# Add helper links to this
hash git 2>&- || { echo >&2 "Git is required but missing. Exiting."; sleep 3; exit; }
hash node 2>&- || { echo >&2 "NodeJS is required but missing. Exiting."; sleep 3; exit; }
hash mysql 2>&- || { echo >&2 "MySQL is required but missing. Exiting."; sleep 3; exit; }
hash php 2>&- || { echo >&2 "PHP is required but missing. Exiting."; sleep 3; exit; }
hash unzip 2>&- || { echo >&2 "Unzip is required but missing. Exiting."; sleep 3; exit; }
hash curl 2>&- || { echo >&2 "cURL is required but missing. Exiting.";sleep 3 exit; }
hash wp 2>&- || { echo >&2 "WP CLI is required but missing. Exiting."; sleep 3; exit; }
hash webpack 2>&- || { echo >&2 "Webpack is required but missing. Exiting."; sleep 3; exit; }
hash composer 2>&- || { echo >&2 "Composer is required but missing. Exiting."; sleep 3; exit; }
hash yarn 2>&- || { echo >&2 "Yarn is required but missing. Exiting."; sleep 3; exit; }

# Get other scripts needed
echo "$(tput setaf 3)Just fetching required scripts...$(tput setaf 9)"
echo

# Get helper functions
. <(curl -s https://bitbucket.org/RobertMarshall0/wordpressshell/raw/cd5f620fc9e0dff8c6ca5b0da38c0f0954382c9a/helpers.sh)

# Get Bitbucket functions
. <(curl -s https://bitbucket.org/RobertMarshall0/wordpressshell/raw/cda783425597291e144eb494743f06a1732ac399/bitbucket.sh)

# Get general functions
. <(curl -s https://bitbucket.org/RobertMarshall0/wordpressshell/raw/77be3795ddc894dcaeaa74a5079f489fd401f242/general.sh)

# Clear the deck
clear

# Here is where we kick off!
projectslug=false
pick_project_slug

projectnicename=false
pick_project_nicename

# Use for database and some string replacements
underslug=$(echo $projectslug|tr '-' '_')

# Lets pick the required platform
echo "$(tput setaf 3)Pick the git platform you wish to use: $(tput setaf 9)"
platform_array=("Bitbucket" "Github")
git_platform=$(create_select_menu ${platform_array[@]})
echo

#Now direct to platform process
if [ $git_platform = "Bitbucket" ]
    then
        run_bitbucket_setup
else
    run_github_setup
fi


sleep 50
