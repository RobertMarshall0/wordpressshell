# Pick the project nice name
pick_project_nicename(){
    echo "$(tput setaf 3)Pick your project name (nice name): $(tput setaf 9)"
    read -e projectnicename
    echo
    # If no name entered, re-ask question
    if [ -z "$projectnicename" ]
        then
        echo "$(tput setaf 1)ERROR: no name entered!$(tput setaf 9)"
        echo "$(tput setaf 1)This is needed to setup the project $(tput setaf 9)"
        echo
        pick_project_nicename
    fi
}


# Pick the project slug
pick_project_slug(){
    echo "$(tput setaf 3)Pick your project slug: $(tput setaf 9)"
    read -e projectslug
    echo
    # If no slug entered, re-ask question
    if [ -z "$projectslug" ]
        then
        echo "$(tput setaf 1)ERROR: no slug entered!$(tput setaf 9)"
        echo "$(tput setaf 1)This is needed to setup the project $(tput setaf 9)"
        echo
        pick_project_slug
    fi
}
