# Function to create select from array
create_select_menu(){
    # Get array from arg
    _array=("$@")

    #Add to select
    select  menu in "${_array[@]}"
    do
        local  retval="$menu"
        echo "$retval"
        break
    done
}
