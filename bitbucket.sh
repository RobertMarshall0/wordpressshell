# Set global vars

# Essential vars
bitbucketuser=false
bitbucketpass=false
bitbucketorg=false
bitbucketprojectkey=false
bitbucketprojectname=false

# Private toggles
bitbucketprojectprivate=false
bitbucketrepoprivate=false

# Toggle on key creation
isbitbucketkeynew=false

bitbucketteam=false


# Ask for Bitbucket username.
get_bitbucket_user(){
    echo "$(tput setaf 3)Please enter your Bitbucket username: $(tput setaf 9)"
    echo "$(tput setaf 3)(your actual username, not email!) $(tput setaf 9)"
    read -e bitbucketuser
    echo
    # If no Bitbucket username entered, re-ask question
    if [ -z "$bitbucketuser" ]
        then
        echo "$(tput setaf 1)ERROR: no Bitbucket username entered!$(tput setaf 9)"
        echo "$(tput setaf 1)This is needed to clone from Bitbucket $(tput setaf 9)"
        echo
        get_bitbucket_user
    fi
}

# Ask for Bitbucket password
get_bitbucket_password(){
    echo "$(tput setaf 3)Please enter your Bitbucket password: $(tput setaf 9)"
    read -s -e bitbucketpass
    echo
    # If no Bitbucket password entered, re-ask question
    if [ -z "$bitbucketpass" ]
        then
        echo "$(tput setaf 1)ERROR: no Bitbucket password entered!$(tput setaf 9)"
        echo "$(tput setaf 1)This is needed to log in to Bitbucket $(tput setaf 9)"
        echo
        get_bitbucket_password
    fi
}

# Ask for Bitbucket organisation name.
get_bitbucket_org(){
    echo "$(tput setaf 3)Please enter the Bitbucket organisation name: $(tput setaf 9)"
    read -e bitbucketorg
    echo
    # If no Bitbucket org username entered, re-ask question
    if [ -z "$bitbucketorg" ]
        then
        echo "$(tput setaf 1)ERROR: no Bitbucket organisation username entered!$(tput setaf 9)"
        echo "$(tput setaf 1)This is needed to set up Bitbucket $(tput setaf 9)"
        echo
        goto "before_bitbucketorg"
    fi
}

# Is a new project folder required?
new_bitbucket_project_required(){
    echo "$(tput setaf 3)Do you need to make a new project folder? (y/n) $(tput setaf 9)"
    read -e yn
    echo
    case $yn in
        [Yy]* ) make_new_bitbucket_project;;
        [Nn]* ) use_existing_bitbucket_key;;
        * ) echo "Please answer y or n."
        new_bitbucket_project_required;;
    esac
}

# Ask for Bitbucket project key
get_bitbucket_project_key(){
    read -e bitbucketprojectkey
    echo
    # If no Bitbucket org username entered, re-ask question
    if [ -z "$bitbucketprojectkey" ]
        then
        echo "$(tput setaf 1)ERROR: no Bitbucket project key entered!$(tput setaf 9)"
        echo "$(tput setaf 1)This is needed to set up Bitbucket $(tput setaf 9)"
        echo
        get_bitbucket_project_key
    fi
}

# Ask for new Bitbucket project name.
# This is for creating a new project
get_bitbucket_project_name(){
    echo "$(tput setaf 3)Please enter the new Bitbucket project name: $(tput setaf 9)"
    read -e bitbucketprojectname
    echo
    # If no Bitbucket project name entered, re-ask question
    if [ -z "$bitbucketprojectname" ]
        then
        echo "$(tput setaf 1)ERROR: no Bitbucket project name entered!$(tput setaf 9)"
        echo "$(tput setaf 1)This is needed to create the repo $(tput setaf 9)"
        echo
        get_bitbucket_project_name
    fi
}

check_existing_bitbucket_project(){

    httpresponse=$(curl GET -s -u $bitbucketuser:$bitbucketpass -w 'HTTPSTATUS:%{http_code}' -H "Content-Type: application/json" https://api.bitbucket.org/2.0/teams/$bitbucketorg/projects/${bitbucketprojectkey^^})
    httpstatus=$(echo "$httpresponse" | tr -d '\n' | sed -e 's/.*HTTPSTATUS://')

    # Handle status codes
    if [ "$httpstatus" = "200" ] || [ "$httpstatus" = "403" ] || [ "$httpstatus" = "404" ]
        then
            # Return status
            local  retval="$httpstatus"
            echo "$retval"
        else
            # Return error
            local  retval="$httpresponse"
            echo "$httpresponse"
    fi
}

# A project does not exist
# Was it a user typo?
# Do we need to make a new one?
project_not_exist_check_typo(){
    # Project doesnt exist
    echo "$(tput setaf 3)This project does not exist$(tput setaf 9)"
    echo "$(tput setaf 3)Do you need to make a new project folder? (press n if this was a typo)(y/n) $(tput setaf 9)"
    read -e yn
    echo
    case $yn in
        [Yy]* ) make_new_bitbucket_project;;
        [Nn]* ) use_existing_bitbucket_key;;
        * ) echo "Please answer y or n."
        project_not_exist_check_typo;;
    esac
}


# Ask the user for an existing key
# then check it
# If it exists, use it
# If it doesnt, we can create it
use_existing_bitbucket_key(){

    # Ask user for existing key name
    echo "$(tput setaf 3)Please enter the existing Bitbucket project key: $(tput setaf 9)"
    echo "$(tput setaf 3)(the client organisation folder in Bitbucket) $(tput setaf 9)"
    get_bitbucket_project_key

    # Check it exists
    echo "$(tput setaf 3)Checking Bitbucket project exists...$(tput setaf 9)"
    echo

    does_bitbucket_project_exist=$(check_existing_bitbucket_project)

    # Handle status codes
    if [ "$does_bitbucket_project_exist" = "200" ]
        then

            # We can use this project! yay
            echo "$(tput setaf 3)Project exists!$(tput setaf 9)"
            echo

            # Let it come back to run_github_setup

    elif  [ "$does_bitbucket_project_exist" = "403" ]
        then

            # Naughty
            echo "$(tput setaf 1)ERROR: You are not authorized$(tput setaf 9)"

            # Send back to start to sign in options
            run_bitbucket_setup

    elif  [ "$does_bitbucket_project_exist" = "404" ]
        then
            project_not_exist_check_typo
    else

        # Got an error, show it and kill
        echo "$(tput setaf 1)ERROR: $(tput setaf 9)"
        echo $does_bitbucket_project_exist
        sleep 5
        exit

    fi

}

make_new_bitbucket_project_key(){

    # Then we need to get a new key
    echo "$(tput setaf 3)Please enter the new Bitbucket project key: $(tput setaf 9)"
    echo "$(tput setaf 3)(the client organisation folder in Bitbucket) $(tput setaf 9)"
    get_bitbucket_project_key

    # Check it does not exist
    echo "$(tput setaf 3)Checking Bitbucket project exists...$(tput setaf 9)"
    echo
    does_bitbucket_project_exist=$(check_existing_bitbucket_project)

    # Handle status codes
    if [ "$does_bitbucket_project_exist" = "200" ]
        then

            # It already exists, we cannot use it!
            echo "$(tput setaf 1)ERROR: That project key already exists$(tput setaf 9)"
            echo

            # Pick another key
            make_new_bitbucket_project_key

    elif  [ "$does_bitbucket_project_exist" = "403" ]
        then

            # Naughty
            echo "$(tput setaf 1)ERROR: You are not authorized$(tput setaf 9)"

            # Send back to start to sign in options
            run_bitbucket_setup

    elif  [ "$does_bitbucket_project_exist" = "404" ]
        then

            # Project doesnt exist, perfect!
            echo "$(tput setaf 3)That key looks good to us. Lets use it!$(tput setaf 9)"
            isbitbucketkeynew=true

    else

        # Got an error, show it and kill
        echo "$(tput setaf 1)ERROR: $(tput setaf 9)"
        echo $does_bitbucket_project_exist
        sleep 5
        exit

    fi

}

# Does new project need to be private
set_bitbucket_project_private(){
    echo "$(tput setaf 3)Is this a private project? (y/n) $(tput setaf 9)"
    read -e yn
    echo
    case $yn in
        [Yy]* ) bitbucketprojectprivate=true;;
        [Nn]* ) bitbucketprojectprivate=false;;
        * ) echo "Please answer y or n."
        set_bitbucket_project_private;;
    esac
}

# Does new repo need to be private
set_bitbucket_repo_private(){
    echo "$(tput setaf 3)Is this a private repository? (y/n) $(tput setaf 9)"
    echo "$(tput setaf 3)(Remeber, if the project folder is private, the repo must also be) $(tput setaf 9)"
    read -e yn
    echo
    case $yn in
        [Yy]* ) bitbucketrepoprivate=true;;
        [Nn]* ) bitbucketrepoprivate=false;;
        * ) echo "Please answer y or n."
        set_bitbucket_repo_private;;
    esac
}

make_new_bitbucket_project(){

    # First we need to get a name
    get_bitbucket_project_name

    # Now make a new key
    make_new_bitbucket_project_key

    # Choose if it is private or not
    set_bitbucket_project_private

    # Now lets make it!
    echo "$(tput setaf 3)Creating Bitbucket project...$(tput setaf 9)"
    echo

    httpresponse=$(curl -X POST -s -u $bitbucketuser:$bitbucketpass -H "Content-Type: application/json" -w 'HTTPSTATUS:%{http_code}' -d "{\"name\":\"$bitbucketprojectname\",\"key\":\"${bitbucketprojectkey^^}\",\"is_private\": $bitbucketprojectprivate}" https://api.bitbucket.org/2.0/teams/$bitbucketorg/projects/)

    httpstatus=$(echo "$httpresponse" | tr -d '\n' | sed -e 's/.*HTTPSTATUS://')

    if [ "$httpstatus" = "201" ]
        then

            # Project has been created
            echo "$(tput setaf 3)Bitbucket project created$(tput setaf 9)"
            echo

            # Let it come back to run_bitbucket_setup

    elif  [ "$httpstatus" = "403" ]
        then

            # Naughty
            echo "$(tput setaf 1)ERROR: You are not authorized$(tput setaf 9)"

            # Send back to start to sign in options
            run_bitbucket_setup

    elif  [ "$httpstatus" = "403" ]
        then
            echo "$(tput setaf 1)ERROR: A team does not exist at this location$(tput setaf 9)"

            # Send back to start to sign in options
            run_bitbucket_setup

    fi

}

make_new_bitbucket_repo(){

    echo "$(tput setaf 3)Lets make the Bitbucket repo...$(tput setaf 9)"
    echo

    #Is this a private repo?
    set_bitbucket_repo_private

    # Set the correct JSON for the curl request
    if [ "$bitbucketrepoprivate" = true ]
        then
            bitbucket_json="{\"scm\":\"git\",\"is_private\":\"true\",\"fork_policy\":\"no_public_forks\",\"project\":{\"key\":\"${bitbucketprojectkey^^}\"}}"
    else
        bitbucket_json="{\"scm\":\"git\",\"project\":{\"key\":\"${bitbucketprojectkey^^}\"}}"
    fi

    httpresponse=$(curl -X POST -s -u $bitbucketuser:$bitbucketpass -H "Content-Type: application/json" -w 'HTTPSTATUS:%{http_code}' -d $bitbucket_json https://api.bitbucket.org/2.0/repositories/$bitbucketorg/$projectslug)

    httpstatus=$(echo "$httpresponse" | tr -d '\n' | sed -e 's/.*HTTPSTATUS://')

    # Proceed if successful.
    if [ "$httpstatus" = "200" ]
        then
            echo "$(tput setaf 3)Bitbucket repository created$(tput setaf 9)"
            echo
        else
            echo $httpresponse
            sleep 5
            exit
    fi

}

# Clone the new repository.
clone_bitbucket_repo(){

    echo "$(tput setaf 3)Cloning repository to local$(tput setaf 9)"
    echo

    git clone https://"$bitbucketuser"@bitbucket.org/"$bitbucketorg"/"$projectslug".git &> /dev/null

    if [ -d "$projectslug" ]

        then

        # Enter the repository directory.
        echo "$(tput setaf 3)Switching to repository directory...$(tput setaf 9)"
        echo

        cd "$projectslug"

    fi
}

run_bitbucket_setup() {

    # Initial details
    get_bitbucket_user
    get_bitbucket_password
    get_bitbucket_org

    # Sends users to make_new_bitbucket_project or
    new_bitbucket_project_required

    # Make repo
    make_new_bitbucket_repo

    # Clone and move into new project folder
    clone_bitbucket_repo

}
