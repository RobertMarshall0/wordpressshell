wordpress_get_from_git(){

    # Fetch and extract the archives from GitHub.
    echo "$(tput setaf 3)Downloading WordPress base repository...$(tput setaf 9)"
    echo

    downloadurl="";

    curl -s -Lo "wordpress-base-master.zip" "$downloadurl"

    unzip "wordpress-base-master" &> /dev/null 2>&1

}

wordpress_move_files(){

    echo "$(tput setaf 3)Moving files into correct folder...$(tput setaf 9)"
    echo

    # Move Base.
    bpdir="wordpress-base-master";
    if [ -d "$bpdir" ]
    	then
    	cp -r "$bpdir"/* .
    fi

    # Remove the archives for good housekeeping.
    rm ./*.zip
    rm -r wordpress-base-master

}

wordpress_replace_strings(){

    # String replacements using input variables
    echo "$(tput setaf 3)Carrying out string replacements...$(tput setaf 9)"
    echo
    for file in $(find .  -type f ! -name '*.woff' ! -name '*.ttf' ! -name '*.eot' ! -name '*.ico' ! -name '' ! -name 'setup.sh' ! -name '.DS_Store' ! -name '*.png' ! -name '*.mo'); do
    	if [[ -f $file ]] && [[ -w $file ]]; then

    		# This is needed in order for this script to work with BSD & GNU sed
    		bakfile=".bak"

    		# Slug - hyphen and underscore replacements.
    		if [[ "$slug" ]]
    		then
    		sed -i$bakfile "s/my-project/$slug/g" "$file"

    		underslug=$(echo $slug|tr '-' '_')
    		sed -i$bakfile "s/my_project/$underslug/g" "$file"
    		fi

    		# Nice Name.
    		if [[ "$nicename" ]]
    		then
    		sed -i$bakfile "s/My Project/$nicename/g" "$file"
    		fi
    	fi
    done

}
